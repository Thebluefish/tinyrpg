extends GridContainer

func _ready():
	connect("visibility_changed", self, "bag_visibility_changed")
	
	pass

func bag_visibility_changed():
	print("bag is ", is_visible())
	
	for i in range(0, player_data.bag.size()):
		get_child(i).set_item(player_data.bag[i])
	
	