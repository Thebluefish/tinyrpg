extends PanelContainer

var item = null

onready var icon_node = get_node("container/margin/icon")
onready var count_node = get_node("container/margin_count/count")

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func set_item(new_item):
	item = new_item
	if item.icon != null:
		icon_node.set_texture(load(item.icon))
	
	if item.count > 1:
		var count = item.count
		var remainder = 0
		var mult = ""
		if (count >= 1000):
			remainder = count % 1000
			count /= 1000
			mult = "k"
		if (count >= 1000):
			remainder = count % 1000
			count /= 1000
			mult = "m"
		if (count >= 1000):
			remainder = count % 1000
			count /= 1000
			mult = "b"
		if (count < 10 and remainder > 100):
			mult = str(".",floor(remainder / 100.0),mult)
		count_node.set_text(str(count, mult))
	else:
		count_node.set_text("")