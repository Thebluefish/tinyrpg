
extends Node

class Data_GameItem:
	export(int) var id = 0
	export(String) var name = ""
	export(Texture) var icon = null
	export(int) var max_stack = null
	export(int, FLAGS, "Head", "Chest", "Legs", "Feet", "Hands", \
	"Cape", "Neck", "Ring", "Primary", "Secondary") var slot = 0

const SLOT_HEAD = 1
const SLOT_CHEST = 2
const SLOT_LEG = 4
const SLOT_FEET = 8
const SLOT_HAND = 16
const SLOT_CAPE = 32
const SLOT_NECK = 64
const SLOT_RING = 128
const SLOT_PRIMARY = 256
const SLOT_SECONDARY = 512

var game_items = []

var SETTINGS_CONFIG_NAME = "user://settings.cfg"
var GAME_SAVE_NAME = "user://game.sav"
var GAME_SAVE_ENCRYPT = false

const game_data_file = "res://assets/data/game_data.json"
var game_data = []

const sample_data_file = "res://assets/data/sample_game_data.json"
const sample_data = {
	items = [
	{ id = 1, name = "gold", icon = "res://assets/sprites/icon.png" },
	{ id = 2, name = "wooden_sword", icon = "res://assets/sprites/icon.png", max_stack = 1, gear_slot = 256 },
	{ id = 3, name = "leather_helmet", icon = "res://assets/sprites/icon.png", max_stack = 1, gear_slot = 1 },
	{ id = 4, name = "leather_chest", icon = "res://assets/sprites/icon.png", max_stack = 1, gear_slot = 2 },
	{ id = 5, name = "wooden round shield", icon = "res://assets/sprites/icon.png", max_stack = 1, gear_slot = 512 },
	{ id = 6, name = "wooden square shield", icon = "res://assets/sprites/icon.png", max_stack = 1, gear_slot = 512 },
	{ id = 7, name = "wooden kite shield", icon = "res://assets/sprites/icon.png", max_stack = 1, gear_slot = 512 },
	{ id = 8, name = "bronze sword", icon = "res://assets/sprites/icon.png", max_stack = 1, gear_slot = 256},
	
	],
	
}

var global_item_store = []

func _ready():
	
	#write out sample data
	var save_file = File.new()
	save_file.open(sample_data_file, File.WRITE)
	save_file.store_string(sample_data.to_json())
	save_file.close()
	
	load_game_data()
	load_game()
	

func get_game_item(item_id):
	for item in global_item_store:
		if item.id == item_id:
			return item

func get_base_item(id):
	for game_item in game_items:
		if ((typeof(id) == TYPE_INT or typeof(id) == TYPE_REAL) and game_item.id == id) \
		or (typeof(id) == TYPE_STRING and game_item.name == id):
			return game_item

func destroy_game_item(item_id):
	for item in global_item_store:
		if item.id == item_id:
			print("Destroyed ",item.count, " ",item.name," as [",item.id, "]!")
			global_item_store.erase(item)
			

func load_game_data():
	
	var load_file = File.new()
	if !load_file.file_exists(game_data_file):
		game_data = sample_data
	else:
		var file_data = {}
		load_file.open(game_data_file, File.READ)
		file_data.parse_json(load_file.get_as_text())
		
		print(file_data)
		
		for item in file_data.items:
			var new_game_item = Data_GameItem.new()
			new_game_item.id = item.id
			new_game_item.name = item.name
			new_game_item.icon = item.icon
			if item.has("max_stack"):
				new_game_item.max_stack = item.max_stack
			if item.has("gear_slot"):
				new_game_item.slot = item.gear_slot
			game_items.append(new_game_item)
		
	load_file.close()
	
	

func save_game():
	var save_file = File.new()
	
	#write out game data
	var save_file = File.new()
	save_file.open(GAME_SAVE_NAME, File.WRITE)
	#save_file.store_string(game_data.to_json())
	save_file.close()

func load_game():
	
	pass

func sort_items(item, other_item):
	if item.id < other_item.id:
		return true
	else:
		return false