extends MarginContainer

var stat_card_scene = preload("res://scenes/stat_card.tscn")
var gear_card_scene = preload("res://scenes/gear_card.tscn")


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func show_card(template, name):
	hide_card()

	var new_card = template.instance()
	add_child(new_card)
	
	return new_card

func hide_card():
	for child in get_children():
		child.queue_free()
		remove_child(child)