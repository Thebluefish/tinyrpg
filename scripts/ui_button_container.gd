extends HBoxContainer

signal button_pressed(button)

var ui_button_scene = preload("res://scenes/ui_button.tscn")
var ui_dir_button_scene = preload("res://scenes/ui_dir_button.tscn")

var left = null
var right = null
var index = 0

func _ready():
	pass
	

func scroll(direction):
	if direction == "left":
		print("left")
		index -= 1
		update()
	elif direction == "right":
		print("right")
		index += 1
		update()
	pass

func button_pressed(button):
	var is_my_child = false
	
	for child in get_children():
		if child == button:
			is_my_child = true
		
		if child != left and child != right and child != button:
			child.set_pressed(false)
			
		
	if is_my_child == true:
		emit_signal("button_pressed", button)

func update():
	.update()
	
	var adjusted_child_count = get_child_count()
	if left != null:
		adjusted_child_count -= 1
	if right != null:
		adjusted_child_count -= 1
	
	if adjusted_child_count <= 4:
		# We can show all children at once
		for child in get_children():
			child.show()
			
		# Remove scroll buttons if present
			if left != null:
				left.queue_free()
				remove_child(left)
				left = null
			if right != null:
				right.queue_free()
				remove_child(right)
				right = null
			
		
	else:
		
		# Add scroll buttons
		if left == null:
			left = ui_dir_button_scene.instance()
			left.set_text("<")
			left.set_h_size_flags(SIZE_FILL)
			left.connect("pressed",self,"scroll", ["left"])
			add_child(left)
		if right == null:
			right = ui_dir_button_scene.instance()
			right.set_text(">")
			right.set_h_size_flags(SIZE_FILL)
			right.connect("pressed",self,"scroll", ["right"])
			add_child(right)
			
		move_child(left, 0)
		move_child(right, get_child_count()-1)
		
		# We can show 3 at a time
		for child in get_children():
			if child != left and child != right:
				child.hide()
		
		if index > get_child_count()-3:
			index = get_child_count()-3
		if index < 2:
			index = 2
		
		get_child(index).show()
		get_child(index-1).show()
		get_child(index+1).show()
		
	
	if left != null:
		left.set_disabled(index <= 2)
	if right != null:
		var childcount = get_child_count()-3
		right.set_disabled(index >= get_child_count()-3)
	
	
func add_button(button_name, button_text, bottom = false):
	
	var new_button = ui_button_scene.instance()
	new_button.set_text(button_text)
	new_button.set_name(button_name)
	new_button.connect("pressed",self,"button_pressed",[new_button])
	if bottom == true:
		add_child(new_button)
	else:
		add_child(new_button)
	
	update()
	
	return new_button

func remove_button(button_name):
	
	var num_removed = 0
	
	for child in get_children():
		if child.get_name() == button_name:
			child.queue_free()
			remove_child(child)
			num_removed += 1
			
		
	
	update()
	
	return num_removed

func clear_buttons():
	
	var num_removed = 0
	
	for child in get_children():
		child.queue_free()
		remove_child(child)
		num_removed += 1
		
	
	update()
	
	return num_removed