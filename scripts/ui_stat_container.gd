extends GridContainer

onready var game_root = get_tree().get_root().get_child(0).get_node("/root/Root")
onready var card_container = game_root.get_node("game_container/game_view/card_container")

func _ready():
	
	for button in get_children():
		button.connect("pressed",self,"button_pressed", [button])
		
	

func button_pressed(button):
	
	for child in get_children():
		if child != button:
			child.set_pressed(false)
			
		
	if button.is_pressed() == true:
		var new_card = card_container.show_card(card_container.stat_card_scene, button.name)
		new_card.set_card(button.name, button.description)
		
	else:
		card_container.hide_card()

func _on_button_container_hide():
	for child in get_children():
		child.set_pressed(false)
		
	card_container.hide_card()
	