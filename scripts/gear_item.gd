extends Button

export(bool) var selected
export(String) var item_name
export(int, FLAGS, "Head", "Chest", "Legs", "Feet", "Hands", \
"Cape", "Neck", "Ring", "Primary", "Secondary") var item_category

onready var name_node = get_node("margin/name")

func _ready():
	pass

func set_name(name):
	name_node.set_text(name)
	