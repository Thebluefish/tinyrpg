extends PanelContainer

onready var top_button_container = get_node("vbox_splitter/top_button_panel/margin/buttons")
onready var bottom_button_container = get_node("vbox_splitter/bottom_button_panel/margin/buttons")

onready var panel_container = get_node("vbox_splitter/panel_container")
onready var panel_default = get_node("vbox_splitter/panel_container/default")


func _ready():
	
	bottom_button_container.add_button("bag", "Bag", true)
	bottom_button_container.add_button("gear", "Gear", true)
	bottom_button_container.add_button("stats", "Stats", true)
	bottom_button_container.add_button("menu", "Menu", true)
	
	top_button_container.connect("button_pressed",bottom_button_container,"button_pressed")
	bottom_button_container.connect("button_pressed",top_button_container,"button_pressed")
	
	top_button_container.connect("button_pressed",self,"button_pressed")
	bottom_button_container.connect("button_pressed",self,"button_pressed")
	
	button_pressed(null)



func button_pressed(button):
	
	for panel in panel_container.get_children():
		if (button != null and button.is_pressed() == true) \
		 and panel.get_name() == button.get_name():
			panel.show()
		else:
			panel.hide()
	
	if button == null or button.is_pressed() == false:
		panel_default.show()