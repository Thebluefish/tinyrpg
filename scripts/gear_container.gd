extends HBoxContainer

onready var left_container = get_node("left")
onready var right_container = get_node("right")
onready var mid_container = get_node("mid_panel/mid_container")
onready var gear_list_container = mid_container.get_node("scroll/gear_list")

onready var game_root = get_tree().get_root().get_child(0).get_node("/root/Root")
onready var card_container = game_root.get_node("game_container/game_view/card_container")

var item_scene = preload("res://scenes/gear_item.tscn")
var item_selected_scene = preload("res://scenes/gear_item_selected.tscn")

# TODO: TRACK ITEMS BY ITEM ID + MODIFIERS
# CURRENTLY ITEM INFO IS PASSED BY NAME
# SWITCH TO { ID, MODIFIER } CLASS FOR TRACKING
# BRB OVERWATCH

func _ready():
	
	for slot in left_container.get_children():
		slot.connect("pressed",self,"slot_pressed", [slot])
	for slot in right_container.get_children():
		slot.connect("pressed",self,"slot_pressed", [slot])

func equip_item(slot, item):
	print("equipped ", item.item_name)
	slot.item = item.item_name
	
	mid_container.hide()

func slot_pressed(slot):
	
	for child in gear_list_container.get_children():
		gear_list_container.remove_child(child)
	
	for child in left_container.get_children():
		if child != slot:
			child.set_pressed(false)
	
	for child in right_container.get_children():
		if child != slot:
			child.set_pressed(false)
			
	if slot.is_pressed():
		mid_container.show()
		
		print(game_data.game_items)
		
		var active_item = ""
		if (slot.item != null):
			add_gear_item(slot, slot.item, slot.item, true)
			active_item = slot.item
		for item in player_data.bag:
			if slot.slot == item.slot:
				add_gear_item(slot, item.name, item.name)
		
	else:
		mid_container.hide()
		
	
func gear_item_pressed(item):
	
	var slot = null
	
	for child in gear_list_container.get_children():
		if child != item:
			child.set_pressed(false)
			
		
	for child in left_container.get_children():
		if child.is_pressed():
			slot = child
		child.set_pressed(false)
	
	for child in right_container.get_children():
		if child.is_pressed():
			slot = child
		child.set_pressed(false)
		
	if item.is_pressed() == false:
		equip_item(slot, item)
		
		card_container.hide_card()
		
	else:
		var new_card = card_container.show_card(card_container.gear_card_scene, item.item_name)
		new_card.set_card(item.item_name, [])
		
	

func add_gear_item(slot, name, display_name, selected = false):
	var new_item = null
	if selected == true:
		new_item = item_selected_scene.instance()
	else:
		new_item = item_scene.instance()
	
	gear_list_container.add_child(new_item)
	new_item.set_name(name)
	new_item.item_name = name
	
	if selected == true:
		new_item.set_pressed(true)
		
		var new_card = card_container.show_card(card_container.gear_card_scene, new_item.item_name)
		new_card.set_card(new_item.item_name, [])
		
	
	new_item.connect("pressed",self,"gear_item_pressed", [new_item])
	