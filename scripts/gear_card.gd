extends PanelContainer

onready var title_node = get_node("margin/vbox/title")
onready var items_node = get_node("margin/vbox/hbox/panel/margin/items")
onready var icon_node = get_node("margin/vbox/hbox/icon")

func _ready():
	print("title_node ", title_node)
	print("items_node ", items_node)
	print("icon_node ", icon_node)
	pass

func set_card(name, items = [], icon = null):
	title_node.set_text(name)
	
	pass