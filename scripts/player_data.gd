
extends Node

const JOB_TYPES = {
    "Combat":0,
    "Slayer":1,
    "":2
}


class GameItem extends "game_data.gd".Data_GameItem:
	var base_id = 0
	var modifier = 0
	var count = 0

# Skills
var skill_attack = 0
var skill_magic = 0
var skill_dexterity = 0
var skill_defense = 0
var skill_holy = 0
var skill_agility = 0

var skill_mining = 0
var skill_smithing = 0
var skill_woodcutting = 0
var skill_construction = 0
var skill_engineering = 0

var skill_fishing = 0
var skill_hunting = 0
var skill_cooking = 0

var bag = []
var bag_limit = 16

func _ready():
	add_item_to_bag(create_item(1,179999))
	add_item_to_bag(create_item(2))
	add_item_to_bag(create_item(3))
	add_item_to_bag(create_item(4))
	add_item_to_bag(create_item(5))
	add_item_to_bag(create_item(6))
	add_item_to_bag(create_item(7))
	add_item_to_bag(create_item(8))
	
	for item in bag:
		print(item.name, " ", item.count)

func create_item(id, count = 1):
	var base_item = game_data.get_base_item(id)
	if base_item == null:
		return
	
	var new_item = GameItem.new()
	game_data.global_item_store.append(new_item)
	
	# Find a free item id
	var new_item_id = 1
	game_data.global_item_store.sort_custom(game_data, "sort_items")
	for item in game_data.global_item_store:
		if new_item_id < item.id:
			new_item_id = item.id
			break
		elif new_item_id == item.id:
			new_item_id += 1
	
	new_item.id = new_item_id
	new_item.base_id = base_item.id
	new_item.name = base_item.name
	new_item.icon = base_item.icon
	new_item.slot = base_item.slot
	new_item.count = count
	return new_item

func merge_game_items(item_from, item_to):
	# Can't merge items of different types
	if item_from.base_id != item_to.base_id:
		return false
		
	# Can't merge items if the new stack is full
	var max_stack = game_data.get_base_item(item_to.base_id).max_stack
	if max_stack != null and item_to.count >= max_stack:
		return false
	
	if max_stack == null:
		item_to.count += item_from.count
		item_from.count = 0
	else:
		var max_stack_diff = max_stack - item_to.count
		if max_stack_diff < item_from.count:
			item_to.count += max_stack_diff
			item_from.count -= max_stack_diff
		else:
			item_to.count += item_from.count 
			item_from.count = 0
	print("from ",item_from.name," x",item_from.count)
	print("to ",item_to.name," x",item_to.count)

func add_item_to_bag(game_item):
	if game_item == null:
		print("Unable to add Unknown Item!")
	elif bag.size() >= bag_limit:
		var item = game_data.get_base_item(item)
		print("Unable to add ",item.name," x",item.count,": bag is full!")
	else:
		for item in bag:
			if game_item.base_id == item.base_id:
				merge_game_items(game_item, item)
		
		if game_item.count > 0:
			bag.append(game_item)
		
		print("Added ",game_item.count, " ",game_item.name," as [",game_item.id, "]!")
		return true
	
	return false

func remove_item_from_bag(id):
	if typeof(id) == TYPE_OBJECT and bag.has(id):
		bag.erase(id)
	elif typeof(id) == TYPE_INT or typeof(id) == TYPE_REAL:
		bag.remove(id)
	else:
		return false
	return true

func remove_items_from_bag(base_id, count, require_count = true):
	
	var base_item = game_data.get_base_item(base_id)
	
	if base_item == null:
		return false
	
	var items_of_type = []
	var total_count_in_bag = 0
	
	for item in bag:
		if item.base_id == base_item.id:
			
			items_of_type.append(item)
			total_count_in_bag += item.count
	
	if require_count == true and count > total_count_in_bag:
		return false
	
	for item in bag:
		if count >= item.count:
			count -= item.count
			item.count = 0
		else:
			item.count -= count
			count = 0
			break
	