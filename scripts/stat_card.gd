extends PanelContainer

onready var title_node = get_node("margin/vbox/title")
onready var description_node = get_node("margin/vbox/hbox/panel/margin/description")
onready var icon_node = get_node("margin/vbox/hbox/icon")

func _ready():
	print("title_node ", title_node)
	print("description_node ", description_node)
	print("icon_node ", icon_node)
	pass

func set_card(name, description, icon = null):
	title_node.set_text(name)
	description_node.set_text(description)
	
	pass